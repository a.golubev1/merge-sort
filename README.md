#Как запустить проект

1) Скачайте все файлы .java и pom.xml и поместите в одну папку.
2) Поместите в эту папку файлы, которые нужно отсортировать
3) Запустите cmd в этой папке и введите:

- mvn package
- java -jar target\merge-sort-0.1.0-jar-with-dependencies.jar /ваши аргументы к программе/

Java version "17.0.2"

Apache Maven 3.8.4

Сторонние библиотеки: Apache Commons IO 2.11.0

<dependency>
    <groupId>commons-io</groupId>
    <artifactId>commons-io</artifactId>
    <version>2.11.0</version>
</dependency>