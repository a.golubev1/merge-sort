import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

public class InputData {
    private String sortOrder;
    private String valueType;
    private PrintWriter outputFile;
    private String outputFileName;
    private ArrayList<Scanner> inputFiles;

    public InputData(String[] args) throws IOException {

        this.inputFiles = new ArrayList<>();
        for (String argument : args) {

            if (argument.equals("-a") || argument.equals("-d")) {
                if (this.sortOrder != null) {
                    throw new IllegalArgumentException("Wrong arguments");
                }
                this.sortOrder = argument;
            } else if (argument.equals("-i") || argument.equals("-s")) {
                if (this.valueType != null) {
                    throw new IllegalArgumentException("Wrong arguments");
                }
                this.valueType = argument;
            } else if ((this.outputFile == null) && argument.contains(".txt")) {
                this.outputFile = new PrintWriter(argument, StandardCharsets.UTF_8);
                this.outputFileName = argument;
            } else {
                File file = new File(argument);
                this.inputFiles.add(new Scanner(file));
            }
        }
        if (this.sortOrder == null) {
            this.sortOrder = "-a";
        }
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public PrintWriter getOutputFile() {
        return outputFile;
    }

    public ArrayList<Scanner> getInputFiles() {
        return inputFiles;
    }

    public String getValueType() {
        return valueType;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public void setInputFiles(ArrayList<Scanner> inputFiles) {
        this.inputFiles = inputFiles;
    }
}
