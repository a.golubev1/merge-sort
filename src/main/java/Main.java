import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        InputData data = new InputData(args);
        MergeSort.initiateSort(data);
    }
}
