import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

public class MergeSort {

    public static void initiateSort(InputData data) throws IOException {
        if (data.getSortOrder().equals("-a")) {
            sortByAccretion(data);
            data.getOutputFile().close();
        } else {
            sortByAccretion(data);
            File fileForOutput = new File(data.getOutputFileName());
            data.getOutputFile().close();
            ReversedLinesFileReader reverseReader = new ReversedLinesFileReader(fileForOutput, Charset.defaultCharset());
            PrintWriter output = new PrintWriter(data.getOutputFileName(), StandardCharsets.UTF_8);
            String curLine = reverseReader.readLine();
            while (curLine != null) {
                output.println(curLine);
                curLine = reverseReader.readLine();
            }
            output.close();
        }
    }

    public static void sortByAccretion(InputData data) {
        int numOfFiles = data.getInputFiles().size();
        ArrayList<String> value = new ArrayList<>();
        for (int i = 0; i < numOfFiles; i++) {
            value.add(data.getInputFiles().get(i).nextLine());
        }
        ResultPair firstCheck = ValueComparator.compareValues(value, data.getValueType().equals("-s"));
        String previousPutValueInOutput = firstCheck.getValue().toString();
        int numOfMistakes = 0;
        while (numOfFiles != 0) {
            ResultPair leastValue = ValueComparator.compareValues(value, data.getValueType().equals("-s"));
            if (ValueComparator.permitToNoteInOutput(previousPutValueInOutput,
                    leastValue.getValue().toString(), data.getValueType().equals("-s"))) {
                data.getOutputFile().println(leastValue.getValue());
                previousPutValueInOutput = leastValue.getValue().toString();
            }
            else{
               numOfMistakes++;
            }
            if (data.getInputFiles().get(leastValue.getCurFile()).hasNextLine()) {
                value.remove(leastValue.getCurFile());
                value.add(leastValue.getCurFile(), data.getInputFiles().get(leastValue.getCurFile()).nextLine());
            } else {
                ArrayList<Scanner> temp = new ArrayList<>();
                for (int i = 0; i < numOfFiles; i++) {
                    if (i != leastValue.getCurFile()) {
                        temp.add(data.getInputFiles().get(i));
                    }
                }
                data.getInputFiles().get(leastValue.getCurFile()).close();
                data.setInputFiles(temp);
                value.remove(leastValue.getCurFile());
                numOfFiles--;
            }
        }
        System.out.print("Number of mistakes in input files: ");
        System.out.print(numOfMistakes);
    }
}
