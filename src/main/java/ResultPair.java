public class ResultPair<E> {
    private final E value;
    private final int curFile;

    public ResultPair(E first, int second) {
        this.value = first;
        this.curFile = second;
    }

    public E getValue() {
        return value;
    }

    public int getCurFile() {
        return curFile;
    }
}
