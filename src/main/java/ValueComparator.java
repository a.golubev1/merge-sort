import java.util.ArrayList;

public class ValueComparator  {
    public static  ResultPair<String> compareValues(ArrayList<String> value, boolean isString) {
        if (isString) {
            String min = value.get(0);
            int curFile = -1;
            for (int i = 0; i < value.size(); i++) {
                if (min.compareTo(value.get(i)) >= 0) {
                    min = value.get(i);
                    curFile = i;
                }
            }
            return new ResultPair<>(min, curFile);
        }
        int min = Integer.MAX_VALUE, curFile = -1;
        for (int i = 0; i < value.size(); i++) {
            int curVal = Integer.parseInt(value.get(i));
            if (min > curVal) {
                min = curVal;
                curFile = i;
            }
        }
        return new ResultPair(min, curFile);
    }

    public static boolean permitToNoteInOutput(String prevValue, String curValue, boolean isString) {
        if (isString) {
            return prevValue.compareTo(curValue) <= 0;
        }
        return Integer.parseInt(prevValue) <= Integer.parseInt(curValue);
    }
}
